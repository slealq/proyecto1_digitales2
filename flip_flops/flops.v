module flops_etapa_1 (
                      input [7:0]      ff1_data_in_0,
                      input [7:0]      ff1_data_in_1,
                      input [7:0]      ff1_data_in_2,
                      input [7:0]      ff1_data_in_3,
                      input            ff1_valid_in_0,
                      input            ff1_valid_in_1,
                      input            ff1_valid_in_2,
                      input            ff1_valid_in_3,
                      output reg [7:0] ff1_data_out_0,
                      output reg [7:0] ff1_data_out_1,
                      output reg [7:0] ff1_data_out_2,
                      output reg [7:0] ff1_data_out_3,
                      output reg       ff1_valid_out_0,
                      output reg       ff1_valid_out_1,
                      output reg       ff1_valid_out_2,
                      output reg       ff1_valid_out_3,
                      input            ff1_clk_1,
                      input            reset_L
                      );

   always @ (posedge ff1_clk_1) begin
      if (!reset_L) begin
         // first muxes step
         ff1_data_out_0 <= 0;
         ff1_data_out_1 <= 0;
         ff1_data_out_2 <= 0;
         ff1_data_out_3 <= 0;
         ff1_valid_out_0 <= 0;
         ff1_valid_out_1 <= 0;
         ff1_valid_out_2 <= 0;
         ff1_valid_out_3 <= 0;

      end else begin
         ff1_data_out_0 <= ff1_data_in_0;
         ff1_data_out_1 <= ff1_data_in_1;
         ff1_data_out_2 <= ff1_data_in_2;
         ff1_data_out_3 <= ff1_data_in_3;
         ff1_valid_out_0 <= ff1_valid_in_0;
         ff1_valid_out_1 <= ff1_valid_in_1;
         ff1_valid_out_2 <= ff1_valid_in_2;
         ff1_valid_out_3 <= ff1_valid_in_3;
      end
   end

endmodule // flops_etapa_1

module flops_etapa_2 (
                      input [7:0] ff2_data_in_0,
                      input [7:0] ff2_data_in_1,
                      input ff2_valid_in_0,
                      input ff2_valid_in_1,
                      output reg [7:0] ff2_data_out_0,
                      output reg [7:0] ff2_data_out_1,
                      output reg ff2_valid_out_0,
                      output reg ff2_valid_out_1,
                      input ff2_clk_2,
                      input reset_L
                      );

   always @ (posedge ff2_clk_2) begin
      if (!reset_L) begin
         ff2_data_out_0 <= 0;
         ff2_data_out_1 <= 0;
         ff2_valid_out_0 <= 0;
         ff2_valid_out_1 <= 0;

      end else begin
         ff2_data_out_0 <= ff2_data_in_0;
         ff2_data_out_1 <= ff2_data_in_1;
         ff2_valid_out_0 <= ff2_valid_in_0;
         ff2_valid_out_1 <= ff2_valid_in_1;
      end // else: !if(!reset_L)
   end // always @ (posedge clk_2)

endmodule // flops_etapa_2
