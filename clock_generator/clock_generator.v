module clock (
              input      clk_16,
              input      reset_L_clk,
              output reg clk_4,
              output reg clk_2,
              output reg clk_1
              );

   reg [3:0]             counter;

   always @ (posedge clk_16) begin
      // increment counter in each clk

      if (!reset_L_clk) begin
         counter <= 0;
         clk_4 <= 0;
         clk_2 <= 0;
         clk_1 <= 0;
      end else begin

         counter <= counter + 1;

         if (counter % 2 == 0) begin
            // clk_4
            clk_4 <= ~ clk_4;
         end // counter % 2

         if (counter % 4 == 0) begin
            // clk_2
            clk_2 <= ~ clk_2;
         end // counter % 4

         if (counter % 8 == 0) begin
            // clk_1
            clk_1 <= ~ clk_1;
         end // counter % 8

      end // else

   end // always

endmodule // clock
