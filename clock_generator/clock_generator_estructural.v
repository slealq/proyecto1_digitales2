/* Generated by Yosys 0.7+655 (git sha1 d36d1193, gcc 8.1.1 -march=x86-64 -mtune=generic -O2 -fstack-protector-strong -fno-plt -fPIC -Os) */

(* top =  1  *)
(* src = "clock_generator.v:1" *)
module clock_estructural(clk_16, reset_L_clk, clk_4, clk_2, clk_1);
  (* src = "clock_generator.v:11" *)
  wire _00_;
  (* src = "clock_generator.v:11" *)
  wire _01_;
  (* src = "clock_generator.v:11" *)
  wire _02_;
  (* src = "clock_generator.v:11" *)
  wire [3:0] _03_;
  wire _04_;
  wire _05_;
  wire _06_;
  wire _07_;
  wire _08_;
  wire _09_;
  wire _10_;
  wire _11_;
  wire _12_;
  wire _13_;
  wire _14_;
  wire _15_;
  wire _16_;
  wire _17_;
  wire _18_;
  wire _19_;
  wire _20_;
  wire _21_;
  wire _22_;
  wire _23_;
  wire _24_;
  wire _25_;
  (* src = "clock_generator.v:6" *)
  output clk_1;
  (* src = "clock_generator.v:2" *)
  input clk_16;
  (* src = "clock_generator.v:5" *)
  output clk_2;
  (* src = "clock_generator.v:4" *)
  output clk_4;
  (* src = "clock_generator.v:9" *)
  wire [3:0] counter;
  (* src = "clock_generator.v:3" *)
  input reset_L_clk;
  NOT _26_ (
    .A(counter[2]),
    .Y(_04_)
  );
  NOT _27_ (
    .A(reset_L_clk),
    .Y(_05_)
  );
  NOT _28_ (
    .A(clk_1),
    .Y(_06_)
  );
  NOT _29_ (
    .A(clk_4),
    .Y(_07_)
  );
  NOR _30_ (
    .A(_05_),
    .B(counter[0]),
    .Y(_03_[0])
  );
  NAND _31_ (
    .A(counter[0]),
    .B(counter[1]),
    .Y(_08_)
  );
  NOT _32_ (
    .A(_08_),
    .Y(_09_)
  );
  NOR _33_ (
    .A(counter[0]),
    .B(counter[1]),
    .Y(_10_)
  );
  NOT _34_ (
    .A(_10_),
    .Y(_11_)
  );
  NAND _35_ (
    .A(reset_L_clk),
    .B(_11_),
    .Y(_12_)
  );
  NOR _36_ (
    .A(_09_),
    .B(_12_),
    .Y(_03_[1])
  );
  NOR _37_ (
    .A(_04_),
    .B(_08_),
    .Y(_13_)
  );
  NAND _38_ (
    .A(_04_),
    .B(_08_),
    .Y(_14_)
  );
  NAND _39_ (
    .A(reset_L_clk),
    .B(_14_),
    .Y(_15_)
  );
  NOR _40_ (
    .A(_13_),
    .B(_15_),
    .Y(_03_[2])
  );
  NAND _41_ (
    .A(_04_),
    .B(_10_),
    .Y(_16_)
  );
  NAND _42_ (
    .A(_06_),
    .B(_16_),
    .Y(_17_)
  );
  NOR _43_ (
    .A(_06_),
    .B(_16_),
    .Y(_18_)
  );
  NAND _44_ (
    .A(reset_L_clk),
    .B(_17_),
    .Y(_19_)
  );
  NOR _45_ (
    .A(_18_),
    .B(_19_),
    .Y(_00_)
  );
  NOR _46_ (
    .A(clk_2),
    .B(_10_),
    .Y(_20_)
  );
  NAND _47_ (
    .A(clk_2),
    .B(_10_),
    .Y(_21_)
  );
  NAND _48_ (
    .A(reset_L_clk),
    .B(_21_),
    .Y(_22_)
  );
  NOR _49_ (
    .A(_20_),
    .B(_22_),
    .Y(_01_)
  );
  NOR _50_ (
    .A(_07_),
    .B(counter[0]),
    .Y(_23_)
  );
  NAND _51_ (
    .A(_07_),
    .B(counter[0]),
    .Y(_24_)
  );
  NAND _52_ (
    .A(reset_L_clk),
    .B(_24_),
    .Y(_25_)
  );
  NOR _53_ (
    .A(_23_),
    .B(_25_),
    .Y(_02_)
  );
  (* src = "clock_generator.v:11" *)
  DFF _54_ (
    .C(clk_16),
    .D(_02_),
    .Q(clk_4)
  );
  (* src = "clock_generator.v:11" *)
  DFF _55_ (
    .C(clk_16),
    .D(_01_),
    .Q(clk_2)
  );
  (* src = "clock_generator.v:11" *)
  DFF _56_ (
    .C(clk_16),
    .D(_00_),
    .Q(clk_1)
  );
  (* src = "clock_generator.v:11" *)
  DFF _57_ (
    .C(clk_16),
    .D(_03_[0]),
    .Q(counter[0])
  );
  (* src = "clock_generator.v:11" *)
  DFF _58_ (
    .C(clk_16),
    .D(_03_[1]),
    .Q(counter[1])
  );
  (* src = "clock_generator.v:11" *)
  DFF _59_ (
    .C(clk_16),
    .D(_03_[2]),
    .Q(counter[2])
  );
endmodule
