`include "clock_generator/clock_generator.v"
`include "phy_tx_conductual.v"
`include "phy_rx.v"

module phy_conductual (
            input [7:0] data_in_0,
            input [7:0] data_in_1,
            input [7:0] data_in_2,
            input [7:0] data_in_3,
            input  valid_in_0,
            input  valid_in_1,
            input  valid_in_2,
            input  valid_in_3,
            input  reset_L,
            input  reset_L_clk,
            input  clk_16,
            output clk_4,
            output clk_2,
            output clk_1
            );

   wire           clk_1, clk_2, clk_4, clk_16;
   wire [1:0]     from_phy_tx_to_phy_rx;

   clock clk ( .clk_16 (clk_16),
               .clk_4 (clk_4),
               .clk_2 (clk_2),
               .clk_1 (clk_1),
               .reset_L_clk (reset_L_clk)
               );


   phy_tx_conductual phy_tx_conductual_ ( .clk_1 (clk_1),
                                          .clk_2 (clk_2),
                                          .clk_4 (clk_4),
                                          .clk_16 (clk_16),
                                          .reset_L (reset_L),
                                          .data_0 (data_in_0),
                                          .data_1 (data_in_1),
                                          .data_2 (data_in_2),
                                          .data_3 (data_in_3),
                                          .valid_0 (valid_in_0),
                                          .valid_1 (valid_in_1),
                                          .valid_2 (valid_in_2),
                                          .valid_3 (valid_in_3),
                                          .data_out_serial (from_phy_tx_to_phy_rx)
                                         );

   phy_rx_conductual phy_rx_conductual_ (
                                         .clk_1 (clk_1),
                                         .clk_2 (clk_2),
                                         .clk_4 (clk_4),
                                         .clk_16 (clk_16),
                                         .reset_L (reset_L),
                                         .data_in (from_phy_tx_to_phy_rx)
                                         );


   endmodule
