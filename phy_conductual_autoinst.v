`include "clock_generator/clock_generator.v"
`include "phy_tx_conductual.v"
`include "phy_rx.v"

module phy_conductual_autoinst (/*AUTOARG*/);


   // Local Variables:
   // verilog-library-files:("./clock_generator/clock_generator.v" "./phy_rx/phy_rx.v")
   // verilog-library-directories:("clock_generator" "." "phy_rx" "phy_tx")
   // End:

   clock clock_ (/*AUTOINST*/
                 // Outputs
                 .clk_4                 (clk_4),
                 .clk_2                 (clk_2),
                 .clk_1                 (clk_1),
                 // Inputs
                 .clk_16                (clk_16),
                 .reset_L_clk           (reset_L_clk));

   phy_rx_conductual phy_rx (/*AUTOINST*/
                             // Outputs
                             .valid_out_0       (valid_out_0),
                             .valid_out_1       (valid_out_1),
                             .valid_out_2       (valid_out_2),
                             .valid_out_3       (valid_out_3),
                             .data_out_L1_0     (data_out_L1_0[7:0]),
                             .data_out_L1_1     (data_out_L1_1[7:0]),
                             .data_out_L1_2     (data_out_L1_2[7:0]),
                             .data_out_L1_3     (data_out_L1_3[7:0]),
                             .clk_4             (clk_4),
                             .clk_2             (clk_2),
                             .clk_1             (clk_1),
                             // Inputs
                             .clk_1             (clk_1),
                             .clk_2             (clk_2),
                             .clk_4             (clk_4),
                             .clk_16            (clk_16),
                             .reset_L           (reset_L),
                             .data_in           (data_in[1:0]),
                             .reset_L_clk       (reset_L_clk));

   phy_tx_conductual phy_tx (/*AUTOINST*/
                             // Outputs
                             .data_out          (data_out[7:0]),
                             .valid_out         (valid_out),
                             .data_out_serial   (data_out_serial[1:0]),
                             // Inputs
                             .clk_1             (clk_1),
                             .clk_2             (clk_2),
                             .clk_4             (clk_4),
                             .clk_16            (clk_16),
                             .reset_L           (reset_L),
                             .data_0            (data_0[7:0]),
                             .data_1            (data_1[7:0]),
                             .data_2            (data_2[7:0]),
                             .data_3            (data_3[7:0]),
                             .valid_0           (valid_0),
                             .valid_1           (valid_1),
                             .valid_2           (valid_2),
                             .valid_3           (valid_3));

   phy_rx_conductual phy_rx (/*AUTOINST*/)


endmodule // phy_conductual_autoinst
