module demux_logic_L1(input clk,
                      input        reset_L,
                      input [7:0]  data_1,
                      input [7:0]  data_2,
                      output [7:0] data_select_0_B,
                      output [7:0] data_select_1_B,
                      output [7:0] data_select_0_C,
                      output [7:0] data_select_1_C,
                      input        valid_1,
                      input        valid_2,
                      output       valid_select_0_B,
                      output       valid_select_1_B,
                      output       valid_select_0_C,
                      output       valid_select_1_C
                    );

   demux demuxB (.clk(clk),
                 .reset_L(reset_L),
                 .data (data_1),
                 .valid (valid_1),
                 .data_out_0 (data_select_0_B),
                 .data_out_1 (data_select_1_B),
                 .valid_de_0 (valid_select_0_B),
                 .valid_de_1 (valid_select_1_B)
                 );

   demux demuxC (.clk(clk),
                 .reset_L(reset_L),
                 .data (data_2),
                 .valid (valid_2),
                 .data_out_0 (data_select_0_C),
                 .data_out_1 (data_select_1_C),
                 .valid_de_0 (valid_select_0_C),
                 .valid_de_1 (valid_select_1_C)
                 );



endmodule
