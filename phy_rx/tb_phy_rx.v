`timescale 1ns / 100ps
`include "phy_rx_tester.v"
`include "phy_rx.v"
`include "phy_conductual.v"
`include "../lib/mycells.v"
`include "phy_rx_estructural.v"


module tb_phy_rx();

   wire [7:0] data;
   wire       reset_L,valid;
   wire       clk_16,clk_4,clk_2,clk_1;
   wire [7:0] data_selec_0;
   wire [7:0] data_selec_1;
   wire       valid_out_0,valid_out_1;
   wire       ffvalid0_cond,ffvalid1_cond,ffvalid2_cond,ffvalid3_cond;
   wire       ffvalid0_est,ffvalid1_est,ffvalid2_est,ffvalid3_est;
   wire [7:0] ffdata0_cond;
   wire [7:0] ffdata1_cond;
   wire [7:0] ffdata2_cond;
   wire [7:0] ffdata3_cond;
   wire [7:0] ffdata0_est;
   wire [7:0] ffdata1_est;
   wire [7:0] ffdata2_est;
   wire [7:0] ffdata3_est;
   wire [7:0] data_out_0;
   wire [7:0] data_out_1;
   wire [7:0] data_out_00;
   wire [7:0] data_out_11;
   wire       valid_de_0, valid_de_1;


    phy_rx_tester tester_rx(.clk_2 (clk_2),
                            .clk_1 (clk_1),
                            .clk_16 (clk_16),
                             .reset_L (reset_L),
                             .data (data),
                             .valid (valid)
                             );

   clock clock_gene(     .clk_16 (clk_16),
                         .clk_4 (clk_4),
                         .clk_2 (clk_2),
                         .clk_1 (clk_1)
                         );

   phy_rx_conductual phy_rx(   .clk_1 (clk_1),
                               .clk_2 (clk_2),
                               .reset_L (reset_L),
                               .data (data),
                               .valid (valid),
                               .valid_out_0 (ffvalid0_cond),
                               .valid_out_1 (ffvalid1_cond),
                               .valid_out_2 (ffvalid2_cond),
                               .valid_out_3 (ffvalid3_cond),
                               .data_out_L1_0 (ffdata0_cond),
                               .data_out_L1_1 (ffdata1_cond),
                               .data_out_L1_2 (ffdata2_cond),
                               .data_out_L1_3 (ffdata3_cond)
                               );


  phy_rx_estructural phy_rx_est (   .clk_1 (clk_1),
                                    .clk_2 (clk_2),
                                    .reset_L (reset_L),
                                    .data (data),
                                    .valid (valid),
                                    .valid_out_0 (ffvalid0_est),
                                    .valid_out_1 (ffvalid1_est),
                                    .valid_out_2 (ffvalid2_est),
                                    .valid_out_3 (ffvalid3_est),
                                    .data_out_L1_0 (ffdata0_est),
                                    .data_out_L1_1 (ffdata1_est),
                                    .data_out_L1_2 (ffdata2_est),
                                    .data_out_L1_3 (ffdata3_est)
                                    );




endmodule