module phy_rx_tester (
                    input clk_2,
                    input clk_1,
                   output reg clk_16,
                   output reg reset_L,
                   output reg [7:0] data,
                   output reg valid
                   );

    reg [2:0] counter;

    initial begin
        $dumpfile("phy_rx.vcd");
        $dumpvars;

        data <= 8'b0000;
        reset_L <= 1'b0;
        valid <= 'b1;

        // start counter with 0
        counter <= 'b0;

       #220;

        reset_L <= 1'b1;

        repeat(100) begin
            @(posedge clk_16);
        end

    // end with 0 in data values
    @(posedge clk_16);
    data = 8'b0;



    $finish;

    end

   always @(posedge clk_2) begin
         counter <= counter + 1;
        data <= data + 1;

         if (counter > 'b1) begin
            counter <= 'b0;
         end

   end




    initial clk_16 <= 0;


    always #16 clk_16 <= ~ clk_16;


    initial begin valid<=1; end


endmodule // phy_rx_tester
