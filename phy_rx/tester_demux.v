module tester_demux (
                   output reg clk,
                   output reg reset_L,
                   output reg [7:0] data,
                   output reg valid
                   );





    reg [2:0] counter;

    initial begin
        $dumpfile("demux.vcd");
        $dumpvars;

        data <= 8'b0000;
        reset_L <= 1'b0;

        // valid values set to 0
        valid <= 'b1;

        // start counter with 0
        counter <= 'b0;

       #50;

        @ (posedge clk);
        reset_L <= 1'b1;

        repeat(100) begin
            @(posedge clk);
        end

    // end with 0 in data values
    @(posedge clk);
    data = 8'b0;



    $finish;

    end // initial begin

   always #40 begin
         counter <= counter + 1;

         if (counter > 'b1) begin
            data <= data + 1;
            counter <= 'b0;
         end // if (

   end// always



    initial clk <= 0;


    always #16 clk <= ~ clk;

    always #100 valid <= ~ valid;

endmodule // phy_tester
