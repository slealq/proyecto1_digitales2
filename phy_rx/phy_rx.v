`include "Lógica_demuxes_L1.v"
`include "Lógica_demuxes_L2.v"
`include "demux.v"
`include "./2b_serial_parallel/2b_serial_parallel_cond.v"

module phy_rx_conductual (
               input        clk_1,
               input        clk_2,
               input        clk_4,
               input        clk_16,
               input        reset_L,
               input [1:0]  data_in,
               output       valid_out_0,
               output       valid_out_1,
               output       valid_out_2,
               output       valid_out_3,
               output [7:0] data_out_L1_0,
               output [7:0] data_out_L1_1,
               output [7:0] data_out_L1_2,
               output [7:0] data_out_L1_3
              );

   wire [7:0]  data_in_8bit, data_out_L2_0, data_out_L2_1,
               flipflop_a, flipflop_b, ffdata0, ffdata1,
               ffdata2, ffdata3;

   wire     valid_out_L2_0, valid_out_L2_1, valid, valid_a, valid_b,
            ffvalid0, ffvalid1, ffvalid2, ffvalid3;


   reg      buffer;

   twob_serial_parallel_cond serial_parallel (
                                              .data_in (data_in),
                                              .clk_4 (clk_4),
                                              .clk_16 (clk_16),
                                              .reset_L (reset_L),
                                              .data_out (data_in_8bit),
                                              .valid (valid)
                                              );

   demux_logic_L2 demux_logic_L2_ (
                                  .clk (clk_4),
                                  .reset_L (reset_L),
                                  .data (data_in_8bit),
                                  .data_select_0 (data_out_L2_0),
                                  .data_select_1 (data_out_L2_1),
                                  .valid (valid),
                                  .valid_select_0 (valid_out_L2_0),
                                  .valid_select_1 (valid_out_L2_1)
                                  );

   flops_etapa_2 ff2 (
                      .ff2_data_in_0 (data_out_L2_0),
                      .ff2_data_in_1 (data_out_L2_1),
                      .ff2_valid_in_0 (valid_out_L2_0),
                      .ff2_valid_in_1 (valid_out_L2_1),
                      .ff2_data_out_0 (flipflop_a),
                      .ff2_data_out_1 (flipflop_b),
                      .ff2_valid_out_0 (valid_a),
                      .ff2_valid_out_1 (valid_b),
                      .ff2_clk_2 (clk_4),
                      .reset_L (reset_L)
                      );

   demux_logic_L1 demux_logic_L1_ (
                                   .clk (clk_2),
                                   .reset_L (reset_L),
                                   .data_1 (flipflop_a),
                                   .data_2 (flipflop_b),
                                   .data_select_0_B (ffdata0),
                                   .data_select_1_B (ffdata1),
                                   .data_select_0_C (ffdata2),
                                   .data_select_1_C (ffdata3),
                                   .valid_1 (valid_a),
                                   .valid_2 (valid_b),
                                   .valid_select_0_B (ffvalid0),
                                   .valid_select_1_B (ffvalid1),
                                   .valid_select_0_C (ffvalid2),
                                   .valid_select_1_C (ffvalid3)
                                   );

   flops_etapa_1 ff1 (ffdata0,
                      ffdata1,
                      ffdata2,
                      ffdata3,
                      ffvalid0,
                      ffvalid1,
                      ffvalid2,
                      ffvalid3,
                      data_out_L1_0,
                      data_out_L1_1,
                      data_out_L1_2,
                      data_out_L1_3,
                      valid_out_0,
                      valid_out_1,
                      valid_out_2,
                      valid_out_3,
                      clk_2,
                      reset_L );

endmodule

