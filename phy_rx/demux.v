// This should be only the conductual description
module demux (
            input            clk,
            input            reset_L,
            input [7:0]      data,
            output reg [7:0]     data_out_0,
            output reg [7:0]     data_out_1,
            input            valid,
            output   reg         valid_de_0,
            output   reg         valid_de_1
            );
   // internal variable
   reg  selector;

   wire [7:0] temp_data_out_1;
   wire [7:0] temp_data_out_0;

   assign temp_data_out_1 = ~(~data);
   assign temp_data_out_0 = ~(~data);



   always @ (posedge clk)
     begin
        // initial values
         if (!reset_L) begin
           // combinational logic
           data_out_0 <= 0;
           data_out_1 <= 0;
           selector <= 0;
           valid_de_1 <= 0;
           valid_de_0 <= 0;

        end

        else begin
            selector <= ~selector;
            if (selector) begin
                valid_de_1 <= valid;
                if (valid) begin
                    data_out_1 <= temp_data_out_1;
                end
                else begin
                    data_out_1 <= data_out_1;
                end
            end
            else begin
                valid_de_0 <= valid;
                if (valid) begin
                    data_out_0 <= temp_data_out_0;
                end
                else begin
                    data_out_0 <= data_out_0;
                end

           end
        end // if not reset

     end

endmodule
