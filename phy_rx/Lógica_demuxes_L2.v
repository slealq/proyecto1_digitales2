module demux_logic_L2 (
              input        clk,
              input        reset_L,
              input [7:0]  data,
              output [7:0] data_select_0,
              output [7:0] data_select_1,
              input        valid,
              output       valid_select_0,
              output       valid_select_1
              );

   demux demuxA (
                  .clk (clk),
                  .reset_L (reset_L),
                  .data (data),
                  .valid (valid),
                  .data_out_0 (data_select_0),
                  .data_out_1 (data_select_1),
                  .valid_de_0 (valid_select_0),
                  .valid_de_1 (valid_select_1)
                  );

endmodule
