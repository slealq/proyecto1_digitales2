
module twob_serial_parallel_cond (
                           input wire [1:0] data_in,
                           input wire       clk_4,
                           input wire       clk_16,
                           input wire       reset_L,
                           output reg [7:0] data_out,
                           output reg       valid
                           );

   reg                                      active;
   reg [7:0]                                buffer;
   reg [7:0]                                postbuffer;
   reg [1:0]                                comcount;
   reg [1:0]                                counter;
   reg                                      reset_L_buffer;

   always @(posedge clk_4) begin
      reset_L_buffer <= reset_L;
   end

   always @(posedge clk_16) begin
      if (!reset_L_buffer) begin
        // when reset is on, set all to 0
         valid <= 0;
         data_out <= 0;
         active <= 0;
         comcount <= 0;
         counter <= 0;
         buffer <= 0;

      end else begin
         // shift data_out to the left, and insert
         // new values on the right
         buffer <= {buffer[5:0], data_in[1:0]};

         counter <= counter + 1;

         if (counter == 'b00) begin
            postbuffer <= buffer;
         end

         if (counter == 'b11) begin
            data_out <= postbuffer;
            // if active and !com then valid = 1, else valid = 0
            if (active == 1 && postbuffer != 'hBC) begin
               valid <= 1;
            end else begin
               valid <= 0;
            end // else

            if (postbuffer == 'hBC && !active) begin
               //one should only look for BCs if active is not up
               //the case when active is up is useless
               comcount <= comcount + 1;

               // if comcount is in 101 binary, active is 1
               if (comcount == 'b11) begin
                  active <= 1;
               end // if comcount

            end // if data ou
         end


      end // else: !if(!reset_L)

   end // always

   // always @(posedge clk) begin
   //    if (data_out == 'hBC && !active) begin
   //       //one should only look for BCs if active is not up
   //       //the case when active is up is useless
   //       comcount <= comcount + 1;

   //       // if comcount is in 101 binary, active is 1
   //       if (comcount == 'b11) begin
   //          active <= 1;
   //       end // if comcount

   //    end // if data out

   // end //always

endmodule
