
module tester (
               output reg [1:0] data_in,
               output reg       clk,
               output reg       reset_L,
               input [7:0]      data_out,
               input            valid
               );

   initial begin
      $dumpfile("simulation.vcd");
      $dumpvars;

      data_in = 0;
      clk = 0;
      reset_L = 0;

      #45 reset_L = 1;

      // put random data withouth bcs first
      repeat (6) begin
         @(posedge clk);

         #5 data_in = data_in + 1;

      end

      // put more bcs than necesary to activate
      repeat (4) begin
         @(posedge clk);

         #5 data_in = 10;

         @(posedge clk);

         #5 data_in = 11;

         @(posedge clk);

         #5 data_in = 11;

         @(posedge clk);

         #5 data_in = 00;
      end

      // now put some data to check if valid is up
      repeat (16) begin
         @(posedge clk);

         #5 data_in = data_in + 1;

      end

      // now send BC again
            // put more bcs than necesary to activate
      repeat (1) begin
         @(posedge clk);

         #5 data_in = 10;

         @(posedge clk);

         #5 data_in = 11;

         @(posedge clk);

         #5 data_in = 11;

         @(posedge clk);

         #5 data_in = 00;
      end // repeat (4)

      // now put some data to check if valid is up
      repeat (24) begin
         @(posedge clk);

         #5 data_in = data_in + 1;

      end


      $finish;

   end // initial begin

   always #40 clk <= ~clk;

endmodule
