# read design
read_verilog 2b_serial_parallel_cond.v
hierarchy -check -top twob_serial_parallel_cond

# the high level stuff
proc; opt; fsm; opt; memory; opt

show

# mapping to internal cell library
techmap; opt

# mapping flip-flops to mycells.lib
dfflibmap -liberty ../../lib/mycells.lib

# mapping logic to mycells.lib
abc -liberty ../../lib/mycells.lib

#cleanup
clean

# write synthesized design
write_verilog 2b_serial_parallel_est.v
#show -lib cmos_cells.v
