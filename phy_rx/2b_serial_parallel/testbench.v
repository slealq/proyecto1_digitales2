`timescale 1ns / 100 ps
`include "2b_serial_parallel_cond.v"
`include "2b_serial_parallel_est.v"
`include "tester.v"
`include "../../lib/mycells.v"

module testbench ();

   wire clk;
   wire [1:0] data_in;
   wire [7:0] data_out_est;
   wire [7:0] data_out_cond;
   wire       reset_L;
   wire       valid_est;
   wire       valid_cond;


   tester prob (
                .data_in (data_in), // de dos en dos bits
                .clk (clk),
                .reset_L (reset_L)
                );

   twob_serial_parallel_cond twob_serial_parallel_ (
                                               .clk (clk),
                                               .data_in (data_in),
                                               .data_out (data_out_cond),
                                               .valid (valid_cond),
                                               .reset_L (reset_L)
                                               );

   twob_serial_parallel_est twob_serial_parallel_est_ (
                                                       .clk (clk),
                                                       .data_in (data_in),
                                                       .data_out (data_out_est),
                                                       .valid (valid_est),
                                                       .reset_L (reset_L)
                                                       );


endmodule
