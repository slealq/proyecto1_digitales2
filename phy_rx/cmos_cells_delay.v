module DFF_delay(C, D, Q);
input C;
input [7:0] D;
output reg [7:0] Q;

always @(posedge C)
	Q <= #258 D;
endmodule

module DFF_1_delay(C, D, Q);
input C;
input D;
output reg Q;

always @(posedge C)
	Q <= #258 D;
endmodule

module DFF_delay_2(C, D, Q);
input C;
input [7:0] D;
output reg [7:0] Q;

always @(posedge C)
	Q <= #20 D;
endmodule

module DFF_1_delay_2(C, D, Q);
input C;
input D;
output reg Q;

always @(posedge C)
	Q <= #20 D;
endmodule

