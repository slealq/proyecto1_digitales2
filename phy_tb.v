`timescale 1ns / 100ps
`include "phy_conductual.v"
`include "phy_estructural.v"
`include "phy_tester.v"
`include "./lib/mycells.v"

module phy_tb;
   wire clk_16;
   wire clk_4, clk_4_est;
   wire clk_2, clk_2_est;
   wire clk_1, clk_1_est;
   wire reset_L, reset_L_clk;
   wire [7:0] data_0;
   wire [7:0] data_1;
   wire [7:0] data_2;
   wire [7:0] data_3;
   wire [7:0] data_out_conductual, data_out_estructural;
   wire valid_0, valid_1,valid_2,valid_3,
        valid_out_conductual, valid_out_estructural;

   phy_tester signal_generator ( .clk_4 (clk_4),
                                 .clk_2 (clk_2),
                                 .clk_1 (clk_1),
                                 .clk_16 (clk_16),
                                 .reset_L (reset_L),
                                 .reset_L_clk (reset_L_clk),
                                 .data_0 (data_0),
                                 .data_1 (data_1),
                                 .data_2 (data_2),
                                 .data_3 (data_3),
                                 .valid_0 (valid_0),
                                 .valid_1 (valid_1),
                                 .valid_2 (valid_2),
                                 .valid_3 (valid_3)
                                 );

   phy_conductual phy_conductual ( .reset_L (reset_L),
                                   .reset_L_clk (reset_L_clk),
                                   .data_in_0 (data_0),
                                   .data_in_1 (data_1),
                                   .data_in_2 (data_2),
                                   .data_in_3 (data_3),
                                   .valid_in_0 (valid_0),
                                   .valid_in_1 (valid_1),
                                   .valid_in_2 (valid_2),
                                   .valid_in_3 (valid_3),
                                   .clk_1 (clk_1),
                                   .clk_2 (clk_2),
                                   .clk_4 (clk_4),
                                   .clk_16 (clk_16)
                                   );

   phy_estructural phy_estructural ( .reset_L (reset_L),
                                     .reset_L_clk (reset_L_clk),
                                     .data_in_0 (data_0),
                                     .data_in_1 (data_1),
                                     .data_in_2 (data_2),
                                     .data_in_3 (data_3),
                                     .valid_in_0 (valid_0),
                                     .valid_in_1 (valid_1),
                                     .valid_in_2 (valid_2),
                                     .valid_in_3 (valid_3),
                                     .clk_1 (clk_1),
                                     .clk_2 (clk_2),
                                     .clk_4 (clk_4),
                                     .clk_16 (clk_16)
                                     );


endmodule
