##
#
#
# @file
# @version 0.1

all : phy

sim_clocks :
	cd phy_tx; make sim_clocks

sim_flopsM1 :
	cd phy_tx; make sim_flopsM1

sim_flopsM2 :
	cd phy_tx; make sim_flopsM2

sim_logicM1 :
	cd phy_tx; make sim_logicM1

sim_logicM2 :
	cd phy_tx; make sim_logicM2

sim_logicM12 :
	cd phy_tx; make sim_logicM12

sim_logicD12 :
	gtkwave ./simulations/sim_logicD12.gtkw

sim_phy_tx :
	cd phy_tx; make sim_phy_tx

sim_phy_rx :
	gtkwave ./simulations/sim_phy_rx.gtkw

sim_phy_summary :
	gtkwave ./simulations/sim_phy_summary.gtkw

sim_clockest:
	cd phy_tx; make sim_clockest

sim_2bserial_parallel :
	gtkwave ./simulations/sim_2bserial_parallel.gtkw

sim_parallel_2bserial :
	gtkwave ./simulations/sim_parallel_2bserial.gtkw

sim_flopsD1 :
	gtkwave ./simulations/sim_flopsD1.gtkw

sim_logicD1:
	gtkwave ./simulations/sim_logicD1.gtkw

sim_flopsD2:
	gtkwave ./simulations/sim_flopsD2.gtkw

sim_logicD2:
	gtkwave ./simulations/sim_logicD2.gtkw

phy : testbench
	gtkwave ./simulations/sim_phy.gtkw

testbench : verilog
	vvp phy.out

verilog : yosys
	iverilog -I ./phy_tx -I ./phy_rx -o phy.out phy_tb.v

yosys : yosys_phy_tx yosys_phy_rx

yosys_phy_tx :
	cd phy_tx; make yosys_phy_tx

yosys_phy_rx :
	cd phy_rx; make yosys

clean :
	rm *.vcd *.out phy_tx_estructural.v

# end
