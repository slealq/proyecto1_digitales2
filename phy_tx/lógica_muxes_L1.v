
module mux_logic_L1(input clk_1,
                    input        reset_L,
                    input [7:0]  data_0,
                    input [7:0]  data_1,
                    input [7:0]  data_2,
                    input [7:0]  data_3,
                    input        valid_0,
                    input        valid_1,
                    input        valid_2,
                    input        valid_3,
                    output [7:0] from_muxA_to_muxC,
                    output [7:0] from_muxB_to_muxC,
                    output       valid_0_muxC,
                    output       valid_1_muxC
                    );

   mux muxA(clk_1, reset_L, data_0, data_1, valid_0, valid_1, valid_0_muxC, from_muxA_to_muxC);
   mux muxB(clk_1, reset_L, data_2, data_3, valid_2, valid_3, valid_1_muxC, from_muxB_to_muxC);

endmodule
