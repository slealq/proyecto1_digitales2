`timescale 1ns / 100ps
`include "../clock_generator/clock_generator.v"
`include "../clock_generator/clock_generator_estructural.v"
`include "phy_tx_tester.v"
`include "phy_tx_conductual.v"
`include "phy_tx_estructural.v"
`include "../lib/mycells.v"

module phy_tb;
   wire clk_16;
   wire clk_4, clk_4_est;
   wire clk_2, clk_2_est;
   wire clk_1, clk_1_est;
   wire reset_L, reset_L_clk;
   wire [7:0] data_0;
   wire [7:0] data_1;
   wire [7:0] data_2;
   wire [7:0] data_3;
   wire [7:0] data_out_conductual, data_out_estructural;
   wire valid_0, valid_1,valid_2,valid_3,
        valid_out_conductual, valid_out_estructural;

   phy_tester signal_generator ( .clk_4 (clk_4),
                                 .clk_2 (clk_2),
                                 .clk_1 (clk_1),
                                 .clk_16 (clk_16),
                                 .reset_L (reset_L),
                                 .reset_L_clk (reset_L_clk),
                                 .data_0 (data_0),
                                 .data_1 (data_1),
                                 .data_2 (data_2),
                                 .data_3 (data_3),
                                 .valid_0 (valid_0),
                                 .valid_1 (valid_1),
                                 .valid_2 (valid_2),
                                 .valid_3 (valid_3)
                                 );

   clock clock_generator ( .clk_16 (clk_16),
                           .clk_4 (clk_4),
                           .clk_2 (clk_2),
                           .clk_1 (clk_1),
                           .reset_L_clk (reset_L_clk)
                           );

   clock_estructural clock_generator_estructural ( .clk_16 (clk_16),
                                                   .clk_4 (clk_4_est),
                                                   .clk_2 (clk_2_est),
                                                   .clk_1 (clk_1_est),
                                                   .reset_L_clk (reset_L_clk)
                                                   );

   phy_tx_conductual phy_conductual ( .clk_1 (clk_1),
                                      .clk_2 (clk_2),
                                      .clk_4 (clk_4),
                                      .clk_16 (clk_16),
                                      .reset_L (reset_L),
                                      .data_0 (data_0),
                                      .data_1 (data_1),
                                      .data_2 (data_2),
                                      .data_3 (data_3),
                                      .valid_0 (valid_0),
                                      .valid_1 (valid_1),
                                      .valid_2 (valid_2),
                                      .valid_3 (valid_3),
                                      .data_out (data_out_conductual),
                                      .valid_out (valid_out_conductual)
                                      );

   phy_tx_estructural phy_estructural ( .clk_1 (clk_1_est),
                                        .clk_2 (clk_2_est),
                                        .clk_4 (clk_4_est),
                                        .clk_16 (clk_16),
                                        .reset_L (reset_L),
                                        .data_0 (data_0),
                                        .data_1 (data_1),
                                        .data_2 (data_2),
                                        .data_3 (data_3),
                                        .valid_0 (valid_0),
                                        .valid_1 (valid_1),
                                        .valid_2 (valid_2),
                                        .valid_3 (valid_3),
                                        .data_out (data_out_estructural),
                                        .valid_out (valid_out_estructural)
                                      );




endmodule
