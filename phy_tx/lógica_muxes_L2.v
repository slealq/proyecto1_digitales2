
module mux_logic_L2 (
              input clk_2,
              input reset_L,
              input [7:0] from_muxA_to_muxC,
              input [7:0] from_muxB_to_muxC,
              input valid_0_muxC,
              input valid_1_muxC,
              output [7:0] data_out,
              output valid_out
              );

   mux muxC(clk_2, reset_L, from_muxA_to_muxC, from_muxB_to_muxC,
   valid_0_muxC, valid_1_muxC, valid_out, data_out);

endmodule
