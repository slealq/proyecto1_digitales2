`include "lógica_muxes_L1.v"
`include "lógica_muxes_L2.v"
`include "../flip_flops/flops.v"
`include "mux.v"
`include "parallel_2b_serial/parallel_2b_serial.v"

module phy_tx_conductual (
               input        clk_1,
               input        clk_2,
               input        clk_4,
               input        clk_16,
               input        reset_L,
               input [7:0]  data_0,
               input [7:0]  data_1,
               input [7:0]  data_2,
               input [7:0]  data_3,
               input        valid_0,
               input        valid_1,
               input        valid_2,
               input        valid_3,
               output [7:0] data_out,
               output       valid_out,
               output [1:0] data_out_serial
              );

   wire [7:0]               ffdata0, ffdata1, ffdata2, ffdata3,
                            from_muxA_to_muxC, from_muxB_to_muxC,
                            flipflop_a, flipflop_b;

   wire                     ffvalid0, ffvalid1, ffvalid2, ffvalid3,
                            valid_0_muxC, valid_1_muxC, valid_a, valid_b;

   flops_etapa_1 ff1(data_0, data_1, data_2, data_3, valid_0, valid_1, valid_2,
                     valid_3, ffdata0, ffdata1, ffdata2, ffdata3, ffvalid0,
                     ffvalid1, ffvalid2, ffvalid3, clk_1, reset_L);

   mux_logic_L1 logic_L1(clk_2,reset_L,ffdata0,ffdata1,ffdata2,ffdata3,ffvalid0,
                         ffvalid1,ffvalid2,ffvalid3,from_muxA_to_muxC,
                         from_muxB_to_muxC,valid_0_muxC,valid_1_muxC);

   flops_etapa_2 ff2(from_muxA_to_muxC, from_muxB_to_muxC, valid_0_muxC,
                     valid_1_muxC, flipflop_a, flipflop_b, valid_a, valid_b,
                     clk_2, reset_L);

   mux_logic_L2 logic_L2(clk_4,reset_L,flipflop_a,flipflop_b,valid_a,valid_b,
                         data_out,valid_out);

   parallel_serial_cond par_to_ser(data_out, clk_16, clk_4, reset_L, valid_out,
                                   data_out_serial);

endmodule
