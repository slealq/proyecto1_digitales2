module phy_tester (
                   input            clk_4,
                   input            clk_2,
                   input            clk_1,
                   output reg       clk_16,
                   output reg       reset_L,
                   output reg       reset_L_clk,
                   output reg [7:0] data_0,
                   output reg [7:0] data_1,
                   output reg [7:0] data_2,
                   output reg [7:0] data_3,
                   output reg       valid_0,
                   output reg       valid_1,
                   output reg       valid_2,
                   output reg       valid_3
                   );

   reg [2:0]                        counter;

   initial begin
      $dumpfile("phy.vcd");
      $dumpvars;

      data_0 <= 8'b0000;
      data_1 <= 8'b1010;
      data_2 <= 8'b0101;
      data_3 <= 8'b1111;
      reset_L <= 1'b0;
      reset_L_clk <= 1'b0;

      // valid values set to 0
      valid_0 <= 'b1;
      valid_1 <= 'b1;
      valid_2 <= 'b1;
      valid_3 <= 'b1;

      // start counter with 0
      counter <= 'b0;

      #30 reset_L_clk <= 1'b1;

      #112 reset_L <= 1'b1;

      repeat(100) begin
         @(posedge clk_1);
      end

      // end with 0 in data values
      @(posedge clk_1);
      data_0 = 8'b0;
      data_1 = 8'b0;
      data_2 = 8'b0;
      data_3 = 8'b0;


      $finish;

   end // initial begin

   always @(posedge clk_1) begin
      data_0 <= data_0 + 1;
      data_1 <= data_1 - 1;
      data_2 <= data_2 + 1;
      data_3 <= data_3 - 1;

   end// always

   initial clk_16 <= 0;


   always #16 clk_16 <= ~ clk_16;

   always #337 valid_0 <= ~ valid_0;
   always #337 valid_1 <= ~ valid_1;
   always #337 valid_2 <= ~ valid_2;
   always #337 valid_3 <= ~ valid_3;

endmodule // phy_tester
