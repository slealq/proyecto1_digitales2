
module tester (
   output reg [7:0] data_in,
   output reg 	    clk,
   output reg 	    reset,
	 output reg				valid
   );

   reg [2:0] cont;

   initial begin
     $dumpfile ("parallel.vcd");
     $dumpvars;
      	valid = 0;
      	reset = 1'b0;
        data_in = 8'b10010011;
        cont = 1'b0;

        @ (posedge clk)begin
        reset <= 1'b1;
        end
        // valid <= 1'b1;
        //
        // repeat (20) begin
        //    @(posedge clk);
        //    cont <= cont + 1;
        //
        //    if (cont > 1'b1) begin
        //       data_in <= data_in + 4;
        //       cont <= 1'b0;
        //    end
        // end
      	// #9 reset = 0;

        #20valid = 1'b1;
        #40valid = 1'b0;
        #24valid = 1'b1;
        #24valid = 1'b0;
      	// repeat (10) begin
        // #10;
        // data_in = 8'b10010011;
        // // data_in = 8'b00110001;
        // end
        // #40 data_in = 8'b10010011;

        // repeat (2) begin
        // #10;
        // data_in = 8'b10010011;
        // end
        // repeat (10) begin
        // data_in = 8'b11011000;
        // end
      	// #16 data_in = 8'b00101000;
        //
      	// #16 data_in = 8'b11111001;
      	// #16 data_in = 8'b01001111;
      	// #16 data_in = 8'b10100110;
      	// #16 data_in = 8'b00111001;
      	// #16 data_in = 8'b10101000;
      	// #16 data_in = 8'b11111001;
      	// #16 data_in = 8'b01001111;
        $finish;
     end
     initial begin
       #38 data_in = 8'b11011000;
     end
     initial clk <= 0;
     always #2 clk = ~clk;

endmodule
