`timescale 1ns/100ps

`include "parallel_2b_serial.v"
`include "tester.v"
`include "synth.v"
`include "cmos_cells.v"

module testbench ();

   //Se definen inputs:
   wire clk;
   wire [7:0] data_in;
   wire valid;

   //Se definen outputs:
   wire       wreset;
   wire [1:0] data_out_cond;
   wire [1:0] data_out_estruc;

   tester prob
     (
      .clk(clk),
      .data_in(data_in),
      .reset(wreset),
      .valid(valid)
      );

   parallel_serial_cond parallel_serial_cond_
     (
      .clk(clk),
      .data_in(data_in),
      .reset(wreset),
      .data_out(data_out_cond),
      .valid(valid)
      );

   parallel_serial_estruc parallel_serial_estruc_
     (
       .clk(clk),
       .data_in(data_in),
       .reset(wreset),
       .data_out(data_out_estruc),
       .valid(valid)
      );

   initial
     begin
    //For GTKWave usage.
    $dumpfile("simulation.vcd");
    $dumpvars();
    #1000 $finish();
     end

endmodule
