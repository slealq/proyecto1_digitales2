/* Generated by Yosys 0.7+655 (git sha1 d36d1193, gcc 8.1.1 -march=x86-64 -mtune=generic -O2 -fstack-protector-strong -fno-plt -fPIC -Os) */

(* top =  1  *)
(* src = "parallel_2b_serial.v:2" *)
module parallel_serial_estruc(data_in, clk_16, clk_4, reset, valid, data_out);
  (* src = "parallel_2b_serial.v:23" *)
  wire [7:0] _000_;
  (* src = "parallel_2b_serial.v:23" *)
  wire [1:0] _001_;
  (* src = "parallel_2b_serial.v:23" *)
  wire _002_;
  wire _003_;
  wire _004_;
  wire _005_;
  wire _006_;
  wire _007_;
  wire _008_;
  wire _009_;
  wire _010_;
  wire _011_;
  wire _012_;
  wire _013_;
  wire _014_;
  wire _015_;
  wire _016_;
  wire _017_;
  wire _018_;
  wire _019_;
  wire _020_;
  wire _021_;
  wire _022_;
  wire _023_;
  wire _024_;
  wire _025_;
  wire _026_;
  wire _027_;
  wire _028_;
  wire _029_;
  wire _030_;
  wire _031_;
  wire _032_;
  wire _033_;
  wire _034_;
  wire _035_;
  wire _036_;
  wire _037_;
  wire _038_;
  wire _039_;
  wire _040_;
  wire _041_;
  wire _042_;
  wire _043_;
  wire _044_;
  wire _045_;
  wire _046_;
  wire _047_;
  wire _048_;
  wire _049_;
  wire _050_;
  wire _051_;
  wire _052_;
  wire _053_;
  wire _054_;
  wire _055_;
  wire _056_;
  wire _057_;
  wire _058_;
  wire _059_;
  wire _060_;
  wire _061_;
  wire _062_;
  wire _063_;
  wire _064_;
  wire _065_;
  wire _066_;
  wire _067_;
  wire _068_;
  wire _069_;
  wire _070_;
  wire _071_;
  wire _072_;
  wire _073_;
  wire _074_;
  wire _075_;
  wire _076_;
  wire _077_;
  wire _078_;
  wire _079_;
  wire _080_;
  wire _081_;
  (* src = "parallel_2b_serial.v:15" *)
  wire [7:0] buffer;
  (* src = "parallel_2b_serial.v:6" *)
  input clk_16;
  (* src = "parallel_2b_serial.v:7" *)
  input clk_4;
  (* src = "parallel_2b_serial.v:5" *)
  input [7:0] data_in;
  (* src = "parallel_2b_serial.v:11" *)
  output [1:0] data_out;
  (* onehot = 32'd1 *)
  wire [3:0] rCurrentState;
  (* src = "parallel_2b_serial.v:8" *)
  input reset;
  (* src = "parallel_2b_serial.v:16" *)
  wire reset_buffer;
  (* src = "parallel_2b_serial.v:9" *)
  input valid;
  (* src = "parallel_2b_serial.v:17" *)
  wire valid_buffer;
  NOT _082_ (
    .A(rCurrentState[1]),
    .Y(_024_)
  );
  NOT _083_ (
    .A(rCurrentState[3]),
    .Y(_025_)
  );
  NOT _084_ (
    .A(buffer[1]),
    .Y(_026_)
  );
  NOT _085_ (
    .A(valid),
    .Y(_027_)
  );
  NOT _086_ (
    .A(valid_buffer),
    .Y(_028_)
  );
  NOT _087_ (
    .A(reset_buffer),
    .Y(_029_)
  );
  NOT _088_ (
    .A(buffer[0]),
    .Y(_030_)
  );
  NOT _089_ (
    .A(buffer[2]),
    .Y(_031_)
  );
  NOT _090_ (
    .A(buffer[4]),
    .Y(_032_)
  );
  NOR _091_ (
    .A(rCurrentState[2]),
    .B(rCurrentState[0]),
    .Y(_033_)
  );
  NAND _092_ (
    .A(_025_),
    .B(_033_),
    .Y(_034_)
  );
  NOR _093_ (
    .A(rCurrentState[1]),
    .B(_034_),
    .Y(_035_)
  );
  NAND _094_ (
    .A(data_out[1]),
    .B(_035_),
    .Y(_036_)
  );
  NOT _095_ (
    .A(_036_),
    .Y(_037_)
  );
  NAND _096_ (
    .A(rCurrentState[3]),
    .B(buffer[7]),
    .Y(_038_)
  );
  NAND _097_ (
    .A(valid_buffer),
    .B(_038_),
    .Y(_039_)
  );
  NAND _098_ (
    .A(_034_),
    .B(_039_),
    .Y(_040_)
  );
  NOR _099_ (
    .A(_024_),
    .B(_027_),
    .Y(_041_)
  );
  NAND _100_ (
    .A(rCurrentState[1]),
    .B(valid),
    .Y(_042_)
  );
  NOR _101_ (
    .A(_026_),
    .B(_042_),
    .Y(_043_)
  );
  NAND _102_ (
    .A(rCurrentState[0]),
    .B(buffer[5]),
    .Y(_044_)
  );
  NAND _103_ (
    .A(rCurrentState[2]),
    .B(buffer[3]),
    .Y(_045_)
  );
  NAND _104_ (
    .A(_044_),
    .B(_045_),
    .Y(_046_)
  );
  NOR _105_ (
    .A(_043_),
    .B(_046_),
    .Y(_047_)
  );
  NAND _106_ (
    .A(_040_),
    .B(_047_),
    .Y(_048_)
  );
  NOR _107_ (
    .A(_037_),
    .B(_048_),
    .Y(_049_)
  );
  NOR _108_ (
    .A(_029_),
    .B(_049_),
    .Y(_001_[1])
  );
  NAND _109_ (
    .A(data_out[0]),
    .B(_035_),
    .Y(_050_)
  );
  NAND _110_ (
    .A(valid_buffer),
    .B(_032_),
    .Y(_051_)
  );
  NAND _111_ (
    .A(rCurrentState[0]),
    .B(_051_),
    .Y(_052_)
  );
  NAND _112_ (
    .A(valid_buffer),
    .B(_031_),
    .Y(_053_)
  );
  NAND _113_ (
    .A(rCurrentState[2]),
    .B(_053_),
    .Y(_054_)
  );
  NAND _114_ (
    .A(_052_),
    .B(_054_),
    .Y(_055_)
  );
  NOR _115_ (
    .A(_030_),
    .B(_042_),
    .Y(_056_)
  );
  NAND _116_ (
    .A(valid_buffer),
    .B(buffer[6]),
    .Y(_057_)
  );
  NOR _117_ (
    .A(_025_),
    .B(_057_),
    .Y(_058_)
  );
  NOR _118_ (
    .A(_056_),
    .B(_058_),
    .Y(_059_)
  );
  NAND _119_ (
    .A(_050_),
    .B(_059_),
    .Y(_060_)
  );
  NOR _120_ (
    .A(_055_),
    .B(_060_),
    .Y(_061_)
  );
  NOR _121_ (
    .A(_029_),
    .B(_061_),
    .Y(_001_[0])
  );
  NAND _122_ (
    .A(_024_),
    .B(buffer[0]),
    .Y(_062_)
  );
  NAND _123_ (
    .A(data_in[0]),
    .B(_041_),
    .Y(_063_)
  );
  NAND _124_ (
    .A(_062_),
    .B(_063_),
    .Y(_065_)
  );
  NAND _125_ (
    .A(reset_buffer),
    .B(_065_),
    .Y(_067_)
  );
  NOT _126_ (
    .A(_067_),
    .Y(_000_[0])
  );
  NAND _127_ (
    .A(_024_),
    .B(buffer[1]),
    .Y(_070_)
  );
  NAND _128_ (
    .A(data_in[1]),
    .B(_041_),
    .Y(_071_)
  );
  NAND _129_ (
    .A(_070_),
    .B(_071_),
    .Y(_072_)
  );
  NAND _130_ (
    .A(reset_buffer),
    .B(_072_),
    .Y(_073_)
  );
  NOT _131_ (
    .A(_073_),
    .Y(_000_[1])
  );
  NAND _132_ (
    .A(_024_),
    .B(buffer[2]),
    .Y(_074_)
  );
  NAND _133_ (
    .A(data_in[2]),
    .B(_041_),
    .Y(_075_)
  );
  NAND _134_ (
    .A(_074_),
    .B(_075_),
    .Y(_076_)
  );
  NAND _135_ (
    .A(reset_buffer),
    .B(_076_),
    .Y(_077_)
  );
  NOT _136_ (
    .A(_077_),
    .Y(_000_[2])
  );
  NAND _137_ (
    .A(_024_),
    .B(buffer[3]),
    .Y(_078_)
  );
  NAND _138_ (
    .A(data_in[3]),
    .B(_041_),
    .Y(_079_)
  );
  NAND _139_ (
    .A(_078_),
    .B(_079_),
    .Y(_080_)
  );
  NAND _140_ (
    .A(reset_buffer),
    .B(_080_),
    .Y(_081_)
  );
  NOT _141_ (
    .A(_081_),
    .Y(_000_[3])
  );
  NAND _142_ (
    .A(_024_),
    .B(buffer[4]),
    .Y(_003_)
  );
  NAND _143_ (
    .A(data_in[4]),
    .B(_041_),
    .Y(_004_)
  );
  NAND _144_ (
    .A(_003_),
    .B(_004_),
    .Y(_005_)
  );
  NAND _145_ (
    .A(reset_buffer),
    .B(_005_),
    .Y(_006_)
  );
  NOT _146_ (
    .A(_006_),
    .Y(_000_[4])
  );
  NAND _147_ (
    .A(_024_),
    .B(buffer[5]),
    .Y(_007_)
  );
  NAND _148_ (
    .A(data_in[5]),
    .B(_041_),
    .Y(_008_)
  );
  NAND _149_ (
    .A(_007_),
    .B(_008_),
    .Y(_009_)
  );
  NAND _150_ (
    .A(reset_buffer),
    .B(_009_),
    .Y(_010_)
  );
  NOT _151_ (
    .A(_010_),
    .Y(_000_[5])
  );
  NAND _152_ (
    .A(_024_),
    .B(buffer[6]),
    .Y(_011_)
  );
  NAND _153_ (
    .A(data_in[6]),
    .B(_041_),
    .Y(_012_)
  );
  NAND _154_ (
    .A(_011_),
    .B(_012_),
    .Y(_013_)
  );
  NAND _155_ (
    .A(reset_buffer),
    .B(_013_),
    .Y(_014_)
  );
  NOT _156_ (
    .A(_014_),
    .Y(_000_[6])
  );
  NAND _157_ (
    .A(_024_),
    .B(buffer[7]),
    .Y(_015_)
  );
  NAND _158_ (
    .A(data_in[7]),
    .B(_041_),
    .Y(_016_)
  );
  NAND _159_ (
    .A(_015_),
    .B(_016_),
    .Y(_017_)
  );
  NAND _160_ (
    .A(reset_buffer),
    .B(_017_),
    .Y(_018_)
  );
  NOT _161_ (
    .A(_018_),
    .Y(_000_[7])
  );
  NOR _162_ (
    .A(rCurrentState[1]),
    .B(_028_),
    .Y(_019_)
  );
  NOR _163_ (
    .A(_041_),
    .B(_019_),
    .Y(_020_)
  );
  NOR _164_ (
    .A(_029_),
    .B(_020_),
    .Y(_002_)
  );
  NAND _165_ (
    .A(_025_),
    .B(reset_buffer),
    .Y(_064_)
  );
  NAND _166_ (
    .A(rCurrentState[1]),
    .B(reset_buffer),
    .Y(_021_)
  );
  NOT _167_ (
    .A(_021_),
    .Y(_066_)
  );
  NAND _168_ (
    .A(rCurrentState[0]),
    .B(reset_buffer),
    .Y(_022_)
  );
  NOT _169_ (
    .A(_022_),
    .Y(_068_)
  );
  NAND _170_ (
    .A(rCurrentState[2]),
    .B(reset_buffer),
    .Y(_023_)
  );
  NOT _171_ (
    .A(_023_),
    .Y(_069_)
  );
  DFF _172_ (
    .C(clk_16),
    .D(_064_),
    .Q(rCurrentState[0])
  );
  DFF _173_ (
    .C(clk_16),
    .D(_069_),
    .Q(rCurrentState[1])
  );
  DFF _174_ (
    .C(clk_16),
    .D(_068_),
    .Q(rCurrentState[2])
  );
  DFF _175_ (
    .C(clk_16),
    .D(_066_),
    .Q(rCurrentState[3])
  );
  (* src = "parallel_2b_serial.v:23" *)
  DFF _176_ (
    .C(clk_16),
    .D(_001_[0]),
    .Q(data_out[0])
  );
  (* src = "parallel_2b_serial.v:23" *)
  DFF _177_ (
    .C(clk_16),
    .D(_001_[1]),
    .Q(data_out[1])
  );
  (* src = "parallel_2b_serial.v:23" *)
  DFF _178_ (
    .C(clk_16),
    .D(_000_[0]),
    .Q(buffer[0])
  );
  (* src = "parallel_2b_serial.v:23" *)
  DFF _179_ (
    .C(clk_16),
    .D(_000_[1]),
    .Q(buffer[1])
  );
  (* src = "parallel_2b_serial.v:23" *)
  DFF _180_ (
    .C(clk_16),
    .D(_000_[2]),
    .Q(buffer[2])
  );
  (* src = "parallel_2b_serial.v:23" *)
  DFF _181_ (
    .C(clk_16),
    .D(_000_[3]),
    .Q(buffer[3])
  );
  (* src = "parallel_2b_serial.v:23" *)
  DFF _182_ (
    .C(clk_16),
    .D(_000_[4]),
    .Q(buffer[4])
  );
  (* src = "parallel_2b_serial.v:23" *)
  DFF _183_ (
    .C(clk_16),
    .D(_000_[5]),
    .Q(buffer[5])
  );
  (* src = "parallel_2b_serial.v:23" *)
  DFF _184_ (
    .C(clk_16),
    .D(_000_[6]),
    .Q(buffer[6])
  );
  (* src = "parallel_2b_serial.v:23" *)
  DFF _185_ (
    .C(clk_16),
    .D(_000_[7]),
    .Q(buffer[7])
  );
  (* src = "parallel_2b_serial.v:23" *)
  DFF _186_ (
    .C(clk_16),
    .D(_002_),
    .Q(valid_buffer)
  );
  (* src = "parallel_2b_serial.v:19" *)
  DFF _187_ (
    .C(clk_4),
    .D(reset),
    .Q(reset_buffer)
  );
endmodule
