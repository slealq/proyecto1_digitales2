# read design
read_verilog parallel_2b_serial.v
hierarchy -check -top parallel_serial_cond

# the high level stuff
proc; opt; fsm; opt; memory; opt

# mapping to internal cell library
techmap; opt

# mapping flip-flops to mycells.lib
dfflibmap -liberty cmos_cells.lib

# mapping logic to mycells.lib
abc -liberty cmos_cells.lib

#cleanup
clean

# write synthesized design
write_verilog synth.v
show -lib cmos_cells.v
