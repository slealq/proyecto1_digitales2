
module parallel_serial_cond
  (
   //inputs
   input wire [7:0] data_in,
   input wire       clk_16,
   input wire       clk_4,
   input wire       reset,
   input wire       valid,
   //outputs
   output reg [1:0] data_out
   );

   reg [1:0]        rCurrentState;
   reg [7:0]        buffer;
   reg              reset_buffer;
   reg              valid_buffer;

   always @(posedge clk_4) begin
      reset_buffer <= reset;
   end

   always @(posedge clk_16) begin
       if (!reset_buffer) begin
          rCurrentState <= 'b00;
          buffer <= 8'b0;
          valid_buffer <= 'b0;
          data_out <= 'b0;
          // cont <=0;
       end else begin
          case (rCurrentState)
            3: begin
               if (valid_buffer) begin
                  data_out[0] <= buffer[6];
                  data_out[1] <= buffer[7];
               end else begin
                  data_out <= 2'b10;
               end //else
               rCurrentState <= 'b00;
            end //case0

            0: begin
               if (valid_buffer) begin
                  data_out[0] <= buffer[4];
                  data_out[1] <= buffer[5];
               end else begin
                  data_out <= 2'b11;
               end //else
               rCurrentState <= 'b01;
            end //case1

            1: begin
               if (valid_buffer) begin
                  data_out[0] <= buffer[2];
                  data_out[1] <= buffer[3];
               end else begin
                  data_out <= 2'b11;
               end //else
               rCurrentState <= 'b10;
            end //case2

            2: begin
               valid_buffer <= valid;
               if (valid) begin
                  buffer <= data_in;
                  data_out[0] <= buffer[0];
                  data_out[1] <= buffer[1];
               end else begin
                  buffer <= 'b0;
                  data_out <= 2'b00;
               end //else
               rCurrentState <= 'b11;
            end

            default:
              begin
                 data_out <= 'b00;
                 //rCurrentState <= 'b00;
              end
          endcase // casex (woResult)

       end // end else

    end // end always

endmodule
