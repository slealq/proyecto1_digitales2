// This should be only the conductual description
module mux (
            input            clk,
            input            reset_L,
            input [7:0]      data_0,
            input [7:0]      data_1,
            input            valid_0,
            input            valid_1,
            output reg valid_out,
            output reg [7:0] data_out
            );
   // internal variable
   reg                       selector;

   always @ (posedge clk)
     begin
        if (reset_L) begin
           selector <= ~selector;
           if (selector) begin
              if (valid_0) begin
                 data_out <= data_0;
                 valid_out <= valid_0;
              end
           end else begin
              if (valid_1) begin
                 data_out <= data_1;
                 valid_out <= valid_1;
              end
           end
        end else begin // if (reset_L)
           data_out <= 0;
           selector <= 0;
           valid_out <= 0;
        end

     end

endmodule
