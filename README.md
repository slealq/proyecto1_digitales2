# Instrucciones de simulación

Para visualizar la simulación del proyecto completo (de phy.v) se debe de utilizar `make phy`, desde el directorio base del proyecto.

Para visualizar las simulaciones en GTKWave, utilizar:
- `make sim_clocks` para simulación de Generador de relojes
- `make sim_clockest` para simulación del Generados de relojes conductual vs estructural.
- `make sim_flopsM1` para simulación de la Etapa de flops antes de la lógica de muxes L1.
- `make sim_logicM1` para simulación de Lógica muxes L1.
- `make sim_flopsM2` para simulación de la Etapa de flops antes de la lógica de muxes L2.
- `make sim_logicM2` para simulación de Lógica muxes L2.
- `make sim_logicM12` para simulación de Lógica de muxes completa. Comparación entre conductual y estructural.
- `make sim_logicD1` para la simulación de Lógica de demuxes L1.
- `make sim_logicD2` para la simulación de Lógica de demuxes L2.
- `make sim_logicD12` para la simulación de Lógica de demuxes completa. Comparación entre conductual y estructural.
- `make sim_flopsD1` para la simulación de la Etapa de flops antes de la lógica de demuxes L1.
- `make sim_flopsD2` para la simulación de la Etapa de flops antes de la lógica de demuxes L2.
- `make sim_parallel_2bserial` para la simulación de Paralelo a 2b Serial.
- `make sim_2bserial_parallel` para la simulación de 2b Serial a Paralelo.
- `make sim_phy_tx` para simulación completa de phy_tx.
- `make sim_phy_summary` para entradas y salidas de phy.


# 1  de octubre

Descripción:
Primera reunión en donde se discute la estructura general del proyecto, roles de los miembros y  confección del primer avance.
    
    
| Integrantes  | Tarea|Descripción  | Duración |
| :---------------: | :---------------:|:---------------:| :---------------:|
| Todos  | Generador de relojes  | Código en Verilog que permita obtener, a partir de una frecuencia de entrada, tres frecuencias de salida, según lo solicitado | 2 horas  |
| Mariela Hernández  | Plan de trabajo  | Descripción del proyecto a ejecutar (roles de miembros y secciones) | 2 horas|
| Pablo Hernández  |  Bitácora | Seguimiento a las tareas que realiza  cada miembro  | 15 minutos |
| Pablo Hernández  |  Lógica de muxes | Código en verilog que permita describir la lógica de muxes   |2 horas |

# 3  de octubre

Descripción:
Segunda reunión en donde se termina la parte funcional del primer avance del proyecto, a excepción de las síntesis con Yosys.
    
    
| Integrantes  | Tarea|Descripción  | Duración|
| :---------------: | :---------------:|:---------------:| :---------------:|
| Stuart Leal y Pablo Hernández   | Lógica de muxes  | Pruebas que permitan verificar el código realizado en Verilog para la lógica de muxes y código para la sincronización (flip-flops para las entradas de cada lógica) | 3 horas  |
| Mariela Hernández  | Plan de trabajo  | Plan de pruebas que se realizarán en el proyecto | 3 horas  |

# 5 de octubre
Descripción:
Último día de trabajo para el primer avance del proyecto.

| Integrantes  | Tarea|Descripción  | Duración|
| :---------------: | :---------------:|:---------------:| :---------------:|
| Mariela Hernández   | Reporte  | Se realiza el reporte del primer avance, describiendo el generador de relojes y la lógica de muxes, así como sus pruebas y resultados | 3 horas  |
| Stuart Leal  | Síntesis con Yosys  | Se sintetizan los .v y se compara el resultado con los conductuales | 2 horas  |

# 10 de octubre
Descripción:
Reunión donde se reparten trabajos y se adelanta el código.

| Integrantes  | Tarea|Descripción  | Duración|
| :---------------: | :---------------:|:---------------:| :---------------:|
| Mariela Hernández   | Paralelo a 2b Serial  | Código conductual en Verilog. Falta la síntesis con Yosys. | 3 horas  |
| Stuart Leal  | 2b Serial a Paralelo | Código conductual en Verilog. Falta la síntesis con Yosys. | 3 horas  |
| Pablo Hernández  | Lógica de demuxes | Código conductual en Verilog. Falta la síntesis con Yosys. | 3 horas  |
| Mariela Hernández  | Corrección Plan de trabajo | Se agrega la recomendación del profesor al Plan de trabajo. | 15 min  |
| Stuart Leal y Pablo Hernández  | Síntesis generador de relojes | Se agrega una señal de reset al generador de relojes y se sintetiza. | 30 min  |

# 12 de octubre
Descripción: 
Finalización del segundo avance del proyecto.

| Integrantes  | Tarea|Descripción  | Duración|
| :---------------: | :---------------:|:---------------:| :---------------:|
| Mariela Hernández   | Síntesis Paralelo a 2b Serial | Síntesis con Yosys. | 1 hora  |
| Pablo Hernández   | Síntesis Lógica de demuxes | Síntesis con Yosys. | 1 hora  |
| Stuart Leal   | Síntesis 2b Serial a Paralelo | Síntesis con Yosys. | 1 hora  |
| Todos   | Reporte | Completar reporte. | 30 min  |
| Mariela Hernández   | Bitácora | Bitácora del trabajo hecho el 10 y 11 de octubre. | 30 min  |

# 17 de octubre
Descripción:
Corrección de errores del segundo avance del proyecto. Inicio del tercer avance.

| Integrantes  | Tarea|Descripción  | Duración|
| :---------------: | :---------------:|:---------------:| :---------------:|
| Mariela Hernández   | Corrección de errores | Mejorar pruebas del bloque Paralelo a 2b Serial. | 1 hora  |
| Pablo Hernández   | Correción de errores | Correción de errores Lógica de demuxes. | 2 horas  |
| Stuart Leal   | Corrección de errores. | Corrección de errores de 2b Serial a Paralelo. | 1 hora  |
| Mariela Hernández   | Reporte | Completar reporte. | 30 min  |
| Mariela Hernández   | Unir módulos | Se inicia a unir los módulos y ordenar los archivos existentes. | 3 horas  |

# 19 de octubre
Descripción:
Finalización del tercer avance del proyecto.

| Integrantes  | Tarea|Descripción  | Duración|
| :---------------: | :---------------:|:---------------:| :---------------:|
| Stuart Leal   | Armar phy, phy_tx y phy_rx | Unir módulos y la totalidad de phy. | 8 horas |
| Mariela Hernández   | Reporte y bitácora | Completar reporte y bitácora. | 3 horas  |

# 23 de octubre
Descripción:
Presentación y reporte del proyecto.

| Integrantes  | Tarea|Descripción  | Duración|
| :---------------: | :---------------:|:---------------:| :---------------:|
| Pablo Hernández   | Resumen del reporte | Escribir resumen del proyecto. | 30 min |
| Mariela Hernández   | Presentación | Crear presentación del proyecto. | 30 min  |


# 24 de octubre

Descripción:
Último avance del proyecto.

| Integrantes  | Tarea|Descripción  | Duración|
| :---------------: | :---------------:|:---------------:| :---------------:|
| Stuart Leal   | AUTOINS | Utilizar AUTOINS para instanciar módulos del proyecto. | 1 hora |
| Mariela Hernández   | Bitácora | Completar bitácora. | 15 min |


